import de.voidplus.leapmotion.*;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

boolean debugMode = true;

LeapMotion leap;
boolean gripOldState = false; //open
boolean gripNewState = false; //open


FileOutputStream fos = null;
DataOutputStream dos = null;

void setup() {
    size(800, 500);
    background(255);
    // ...
    String btDevName = "/dev/tty.EV3-SerialPort"; // actual device for MacOsX
    String debugFile = "/Users/abder/dev/tty.EV3-SerialPort"; //my debug file (adjust it to your requirements)

    String strFilePath = btDevName;

    if (debugMode) {
        strFilePath = debugFile;
    } else {
        strFilePath = btDevName;
    }

    try {
        //create FileOutputStream object
        fos = new FileOutputStream(strFilePath);
        dos = new DataOutputStream(fos);

    } catch (IOException e) {
        System.out.println("IOException : " + e);
    }

    leap = new LeapMotion(this);
}

void draw() {
    background(255);
    // ...
    int fps = leap.getFrameRate();


    // ========= HANDS =========

    for (Hand hand: leap.getHands()) {


        // ----- BASICS -----

        int hand_id = hand.getId();
        PVector hand_position = hand.getPosition();
        PVector hand_stabilized = hand.getStabilizedPosition();
        PVector hand_direction = hand.getDirection();
        PVector hand_dynamics = hand.getDynamics();
        float hand_roll = hand.getRoll();
        float hand_pitch = hand.getPitch();
        float hand_yaw = hand.getYaw();
        boolean hand_is_left = hand.isLeft();
        boolean hand_is_right = hand.isRight();
        float hand_grab = hand.getGrabStrength();
        float hand_pinch = hand.getPinchStrength();
        float hand_time = hand.getTimeVisible();
        PVector sphere_position = hand.getSpherePosition();
        float sphere_radius = hand.getSphereRadius();

        // ----- DRAWING -----

        hand.draw();
        hand.drawSphere();

        //----------------------
        //My routine for grip

        if (hand.isLeft()) {

            float gripStrenght = hand.getGrabStrength();

            if (gripStrenght > 0.5) {
                //close
                println("I'm closing");
                gripNewState = true; //closed

                if (gripNewState != gripOldState) {

                    //write command

                    sendBluetoothCmd(stdCommand("cmd_action_grip"), 2000);
                    sendBluetoothCmd(stdCommand("cmd_stop_A"), 10);

                    //refresh old state
                    gripOldState = true;
                }

            } else {

                //open
                println("I'm openning");
                gripNewState = false; //opened

                if (gripNewState != gripOldState) {
                    //write command

                    sendBluetoothCmd(stdCommand("cmd_release_grip"), 2000);
                    sendBluetoothCmd(stdCommand("cmd_stop_A"), 10);

                    //refresh old state
                    gripOldState = false;
                }
            }
        }

        // ========= ARM =========



        // ========= FINGERS =========
        //my routine for moving the robot
        if (hand.isRight()) {

            if (hand.hasArm()) {
                Arm arm = hand.getArm();
                PVector arm_wrist_pos = arm.getWristPosition();
                println("l0= " + arm_wrist_pos + ";");

                println("hand direction: " + hand.getDirection());
                float roll = hand.getRoll();
                float pitch = hand.getPitch();
                float yaw = hand.getYaw();
                println("pitch: " + pitch + " yaw: " + yaw + " roll: " + roll);

                if (hand.getPinchStrength() < 0.75) {
                    if (yaw > -10 && yaw < 10) {
                        println("forward");
                    }

                    if (yaw > 10) {
                        println("Right");
                    }

                    if (yaw < -10) {
                        println("Left");
                    }

                } else {
                    println("Backward");
                }


            }

            //-------
            float gripStrenght = hand.getGrabStrength();

            if (gripStrenght > 0.5) {

                //use this to stop instead (should be the initial state)

                sendBluetoothCmd(stdCommand("cmd_move_fwd_BC"), 3000);
            }

            //-------
            for (Finger finger: hand.getFingers()) {
                // Alternatives:
                // hand.getOutstretchedFingers();
                // hand.getOutstretchedFingersByAngle();

                // ----- BASICS -----

                int finger_id = finger.getId();
                PVector finger_position = finger.getPosition();
                PVector finger_stabilized = finger.getStabilizedPosition();
                PVector finger_velocity = finger.getVelocity();
                PVector finger_direction = finger.getDirection();
                float finger_time = finger.getTimeVisible();

                // ----- SPECIFIC FINGER -----

                switch (finger.getType()) {
                    case 0:
                        // System.out.println("thumb");
                        //System.out.println("l1= " + finger.getStabilizedPosition()+";");
                        break;
                    case 1:
                        // System.out.println("index");

                        //System.out.println("l2= " + finger.getStabilizedPosition()+";");
                        break;
                    case 2:
                        // System.out.println("middle");
                        //System.out.println("l3= " + finger.getStabilizedPosition()+";");
                        break;
                    case 3:
                        // System.out.println("ring");
                        //System.out.println("l4= " + finger.getStabilizedPosition()+";");
                        break;
                    case 4:
                        // System.out.println("pinky");
                        //System.out.println("l5= " + finger.getStabilizedPosition()+";");
                        break;
                }

                // ----- DRAWING -----

                finger.draw(); // = drawBones()+drawJoints()
                //finger.drawBones();
                //finger.drawJoints();


                // ----- TOUCH EMULATION -----

                int touch_zone = finger.getTouchZone();
                float touch_distance = finger.getTouchDistance();

                switch (touch_zone) {
                    case -1: // None
                        break;
                    case 0: // Hovering
                        // println("Hovering (#"+finger_id+"): "+touch_distance);
                        break;
                    case 1: // Touching
                        // println("Touching (#"+finger_id+")");
                        break;
                }
            }

        }

        // ========= TOOLS =========

        for (Tool tool: hand.getTools()) {


            // ----- BASICS -----

            int tool_id = tool.getId();
            PVector tool_position = tool.getPosition();
            PVector tool_stabilized = tool.getStabilizedPosition();
            PVector tool_velocity = tool.getVelocity();
            PVector tool_direction = tool.getDirection();
            float tool_time = tool.getTimeVisible();


            // ----- DRAWING -----

            // tool.draw();


            // ----- TOUCH EMULATION -----

            int touch_zone = tool.getTouchZone();
            float touch_distance = tool.getTouchDistance();

            switch (touch_zone) {
                case -1: // None
                    break;
                case 0: // Hovering
                    // println("Hovering (#"+tool_id+"): "+touch_distance);
                    break;
                case 1: // Touching
                    // println("Touching (#"+tool_id+")");
                    break;
            }
        }
    }


    // ========= DEVICES =========

    for (Device device: leap.getDevices()) {
        float device_horizontal_view_angle = device.getHorizontalViewAngle();
        float device_verical_view_angle = device.getVerticalViewAngle();
        float device_range = device.getRange();
    }
}


// ========= CALLBACKS =========

void leapOnInit() {
    // println("Leap Motion Init");
}
void leapOnConnect() {
    // println("Leap Motion Connect");
}
void leapOnFrame() {
    // println("Leap Motion Frame");
}
void leapOnDisconnect() {
    // println("Leap Motion Disconnect");

}
void leapOnExit() {
    // println("Leap Motion Exit");
    try {
        dos.close();
    } catch (IOException e) {
        System.out.println("IOException : " + e);
    }
}

String stdCommand(String cmd) {

    HashMap < String, String > hm = new HashMap < String, String > ();

    char start_motor[] = {
        0x0D, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0xA4, 0x00, 0x01, 0x81, 0x14, 0xA6, 0x00, 0x01
    };
    hm.put("cmd_action_grip", String.valueOf(start_motor));

    char stop_motor[] = {
        0x09, 0x00, 0x01, 0x00, 0x80, 0x00, 0x00, 0xA3, 0x00, 0x01, 0x00
    };
    hm.put("cmd_stop_A", String.valueOf(stop_motor));

    char move_ev3[] = {
        0x12, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0xAE, 0x00, 0x06, 0x81, 0x32, 0x00, 0x82, 0x84, 0x03, 0x82, 0xB4, 0x00, 0x01
    };
    hm.put("cmd_move_fwd_BC", String.valueOf(move_ev3));

    char release_grip[] = {
        0x0d, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0xa4, 0x00, 0x01, 0x81, 0xec, 0xa6, 0x00, 0x01
    };
    hm.put("cmd_release_grip", String.valueOf(release_grip));

    return hm.get(cmd);
}

void sendBluetoothCmd(String cmd, int delayAfter) {
    try {
        dos.writeBytes(cmd);
        if (debugMode) {
            delay(0);
        } else {
            delay(delayAfter);
        }

    } catch (IOException e) {
        println("IOException : " + e);
    }
}